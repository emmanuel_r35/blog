/* global Backendless, Posts, Handlebars, moment */

$(function (){
    var APPLICATION_ID = "9DA97C77-B491-D7F4-FFD9-BB3742A92C00",
        SECRET_KEY = "3551737B-40CD-6381-FF67-4CE09DA2A200",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
   
    
   function Posts (args){
      args = args || {};
      this.title = args.title || {};
      this.content = args.content || "";
      this.authorEmail = args.authorEmail || "";
   }
   
 
   
});